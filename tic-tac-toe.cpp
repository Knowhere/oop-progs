#include <iostream>
#include <stdlib.h>

using namespace std;

struct Tic_tac_toe{
	char arr [3][3];

	Tic_tac_toe(){
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				arr[i][j] = '.';
			}
		}
	}

	int insert(int , int , char );
	void clear();
	int check(char mark);
};

void Tic_tac_toe::clear(){
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			arr[i][j] = '.';
		}
	}
}


int Tic_tac_toe::insert(int x, int y, char mark){
	if(arr[x][y] == '.'){
		arr[x][y] = mark;
		return 0;
	}else{
		cout<<"invalid move! Retry"<<endl;
		return 1;
	}
}

int win_row(char ch1, char ch2, char ch3) {
	if ((ch1 == ch2) && (ch1 == ch3)) {
		if (ch1 == 'X' || ch1 == 'O')
		{
			return 1;
		}
	}
	return 0;
}

int Tic_tac_toe::check(char mark){
	for (int i = 0; i < 3; i++) {
		if (win_row(arr[i][0], arr[i][1], arr[i][2])) {
			return 1;
		}
	}
	// Loop through the columns
	for (int i = 0; i < 3; i++) {
		if (win_row(arr[0][i], arr[1][i], arr[2][i])) {
			return 1;
		}
	}
	// Check diagonals
	if (win_row(arr[0][0], arr[1][1], arr[2][2])) {
		return 1;
    }

	if (win_row(arr[0][2], arr[1][1], arr[2][0])) {
		return 1;
	}
	return 0;

}

int main()
{
	Tic_tac_toe t;
	char mark;
	int a, ret, k;
	cout<<"Welcome to Tic-tac-toe!"<<endl<<endl;
	for (k = 0; k < 9; k++)
	{
		mark = (k%2 == 0) ? 'X' : 'O';
		for (int i = 0; i < 3; i++)
		{
			cout<<"   --------------"<<endl;
			for (int j = 0; j < 3; j++)
			{
				cout<<"  | ";
				if(t.arr[i][j] == '.'){
					cout<<(3*i)+j+1; 
				}else{
					if(t.arr[i][j] == 'X'){
						cout<<"X";
					}else{
						cout<<"O";
					}
				}
				
			}
			cout<<"  |  "<<endl;
		}
		cout<<"   --------------"<<endl<<endl;
		cout<<"Player "<<(k%2)+1<<" Make your Move : \t\t";
		cin>>a;
		ret = t.insert( a/3, (a%3)-1, mark );
		if(ret == 1)
			k--;
		if(t.check(mark) == 1){
			cout<<"\""<<mark<<"\" Wins!"<<endl<<endl;
			break;
		}
	}
	if(k == 9)
		cout<<"Game Draw";
	return 0;
}