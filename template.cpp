#include <iostream>
#include <string>

using namespace std;

template <typename T>
void swap(T *ptr1, T *ptr2)
{
	T temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

template <typename T>
void sort(T arr[], int n)
{ 
	for (int i = 0; i < n-1; i++)
	{
		int min = i;
		for (int j = i+1; j < n; j++)
			if (arr[j] < arr[min])
				min = j;
 		swap(&arr[min], &arr[i]);
	}
}

template <typename T>
void display(T arr[], int size)
{
	for (int i=0; i < size; i++)
		cout<<arr[i]<<" \t";
	cout<<endl;
}

template <typename T>
void accept(T arr[], int size)
{
	for (int i=0; i < size; i++)
		cin>>arr[i];
	cout<<endl;
}

int main () {
	int arr[50], size;
	cout<<"For Int array:"<<endl<<"Enter array size: \t";
	cin>>size;
	accept(arr, size);
	sort(arr, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);

	float arr2[50];
	cout<<"For Float array:"<<endl<<"Enter array size: \t";
	cin>>size;
	accept(arr2, size);
	sort(arr2, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr2, size);

	return 0;
}