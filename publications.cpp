#include<iostream>
using namespace std;

class publication{
	public:
	char name[50], author[50];
	float price;
	virtual void accept() = 0;
	virtual void display() = 0;
};

class book: public publication{
	int page_count;
	public:
	void accept()
	{
		cout<<"Enter Name:";
		cin>>name;
		cout<<"Enter Author's Name:";
		cin>>author;
		cout<<"Enter Price:";
		cin>>price;
		cout<<"Enter Page count";
		cin>>page_count;
	}
	
	void display()
	{
		cout<<"Name:\t"<<name<<"\n";
		cout<<"Author's Name:\t"<<name<<"\n";
		cout<<"Price:\t"<<price<<"\n";
		cout<<"Page count: \t"<<page_count<<"\n";
	}
};

class tape: public publication{
	int time;
	public:
	void accept()
	{
		cout<<"Enter Name:";
		cin>>name;
		cout<<"Enter Author's Name:";
		cin>>author;
		cout<<"Enter Price:";
		cin>>price;
		cout<<"Enter tape time in minutes";
		cin>>time;
	}
	
	void display()
	{
		cout<<"Name:\t"<<name<<"\n";
		cout<<"Author's Name:\t"<<name<<"\n";
		cout<<"Price:\t"<<price<<"\n";
		cout<<"Tape Time : \t"<<time<<"(mins)\n";
	}
};

int main()
{
book b1;
tape t1;
cout<<"Enter Book Details\n";
b1.accept();
b1.display();
cout<<"\nEnter Audio tape Details\n";
t1.accept();
t1.display();
return 0;
}
