#include <iostream>
#include <fstream>
#include <string.h>
#include<stdlib.h>
#include <iomanip>
using namespace std;

class telephone_directory{
	char name[20],phone[15];
public:
	void getdata();
	void showdata();
	char *getname(){ return name; }
	char *getno(){ return phone; }
	void update(char *dir_name,char *dir_phone){
	strcpy(name,dir_name);
	strcpy(phone,dir_phone);
	}
};

void telephone_directory :: getdata(){
	cout<<"\nEnter Name : ";
	cin>>name;
	cout<<"Enter Phone No. : ";
	cin>>phone;
}

void telephone_directory :: showdata(){
	cout<<endl;
	cout<<name;
	cout<<phone;
}


int main(){
	telephone_directory record;
	fstream file;
	file.open("telephone_directory.txt", ios::ate | ios::in | ios::out);
	char ch,dir_name[20],dir_phone[6];
	int choice,flag=0;
	int count=0;
	while(1){
		cout<<"\n*********** Menu: ***********\n1. Add New Record\n2. Display All Records\n3. Search Telephone No.\n4. Search Person Name\n5. Update Telephone No.\n6. Exit\nEnter your choice : ";
		cin>>choice;
		switch(choice){
			case 1 : //Add Details
				record.getdata();
				file.write((char *) &record, sizeof(record));
				break;

			case 2 : //Display Details
				file.seekg(0,ios::beg);
				cout<<"\n\nDisplaying Records in Phone Book\n";
				while(file){
					file.read((char *) &record, sizeof(record));
					if(!file.eof())
						record.showdata();
				}
				cout<<endl;
				break;

			case 3 : //Search Tel. no. in phone book
				cout<<"\n\nEnter Name : ";
				cin>>dir_name;
				file.seekg(0,ios::beg);
				flag=0;
				while(file.read((char *) &record, sizeof(record))) {
					if(strcmp(dir_name,record.getname())==0) {
						flag=1;
						record.showdata();
					}
				}
				if(flag==0)
					cout<<"\n\n------Record Not found------\n";
				break;

				case 4 : //Search name in phone book
				cout<<"\n\nEnter Telephone No : ";
				cin>>dir_phone;
				file.seekg(0,ios::beg);
				flag=0;
				while(file.read((char *) &record, sizeof(record)))
				{
				if(strcmp(dir_phone,record.getno())==0)
				{
				flag=1;
				record.showdata();
				}
				}
				if(flag==0)
				cout<<"\n\n------Record Not found------\n";
				break;
		
			case 5 : //Update Telephone No.
				cout<<"\n\nEnter Name : ";
				cin>>dir_name;
				file.seekg(0,ios::beg);
				flag=0;
				count=0;
				while(file.read((char *) &record, sizeof(record)))
				{
					count++;
					if(strcmp(dir_name,record.getname())==0){
						flag=1;
						break;
					}
				}
				if(flag==0)
					cout<<"\n\n------Record Not found------\n";
				else {
					int position = (count-1) * sizeof(record);
					cin.get(ch);
					if(file.eof())
		
						cout<<"Enter New Telephone No : \t";
						cin>>dir_phone;
						file.seekp(position);
						record.update(dir_name,dir_phone);
						file.write((char *) &record, sizeof(record));
						file.flush();
				}
				break;
			default :
				exit(0);
			}
	}
return 0;
}