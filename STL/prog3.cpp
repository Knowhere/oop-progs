#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <iomanip>

using namespace std;

class Date {
	private:
		short month, day, year;
	public: 
		Date();
		Date(int, int, int);
		void print();
};

Date::Date(){
	month = 1;
	day = 1;
	year = 1998;
}

Date::Date( int m, int d, int y) {
	month = (short) m;
	day   = (short) d;
	year  = (short) y;
}

void Date::print() {
	cout << month << '-' << day << '-' << year;
}

class Person {
	string name;
	Date birthdate;
	string phone_no;
public:
	Person(){
		birthdate = Date(1,1,1998);
		accept();
	}
	void accept(){
		cout<<"Enter Your Name:\t";
		cin>>name;
		cout<<"Enter Your Birthdate:\n";
		short m,d,y;
		cout<<"Enter Month\t";
		cin>>m;
		cout<<"Enter Day\t";
		cin>>d;
		cout<<"Enter Year\t";
		cin>>y;
		birthdate = Date(m,d,y);
		cout<<"Enter Your Phone No:\t";
		cin>>phone_no;
	}
	void display(){
		cout<<name<<"\t\t";
		birthdate.print();
		cout<<"\t";
		cout<<phone_no<<endl;
	}

	bool operator<(const Person &p1){
 	   return this->name < p1.name;
	}
	string getname(){
		return name;
	}
};

int main(){
	int choice, flag;
	string name;
	Person *ptr;
	vector <Person> vp;
	vector <Person>::iterator vp_it = vp.begin();
	while(1){
		cout<<"1. Enter new Record \n2. display all Elements \n3. Sort \n4. Search by name\n5. Exit"<<endl<<endl;
		cout<<"Enter Your Choice: \t";
		cin>>choice;
		switch(choice){
			case 1:
				ptr = new Person();
				vp.push_back(*ptr);
				break;
			case 2:
				if(vp.empty() == 0){
					cout<<"Displaying all Elements"<<endl<<endl;
					cout<<"Name\t\t";
					cout<<"Birthdate\t";
					cout<<"Phone No."<<endl<<endl;
					for(vp_it = vp.begin();vp_it != vp.end(); vp_it++){
						vp_it->display();
						cout<<endl;
					}
				}else{
					cout<<endl<<"No Elements present!"<<endl<<endl;
				}
				break;
			case 3:
				if(vp.empty() == 0){
					sort(vp.begin(), vp.end());
					cout<<"Displaying sorted Elements"<<endl<<endl;
					cout<<"Name\t\t";
					cout<<"Birthdate\t";
					cout<<"Phone No."<<endl<<endl;
					for(vp_it = vp.begin();vp_it != vp.end(); vp_it++){
						vp_it->display();
						cout<<endl;
					}
				}else{
					cout<<endl<<"No Elements present!"<<endl<<endl;
				}
				break;
			case 4:
				if(vp.empty() == 0){
					cout<<"\n\nEnter Name : ";
					cin>>name;
					flag=0;
					for( vp_it = vp.begin() ; vp_it != vp.end() ; vp_it++ ) {
						if( name.compare(vp_it->getname()) == 0 ) {
							flag=1;
							cout<<"------Record found------"<<endl<<endl;
							cout<<"Name\t\t";
							cout<<"Birthdate\t";
							cout<<"Phone No."<<endl<<endl;
							vp_it->display();
							break;
						}
					}
					if(flag==0)
						cout<<"\n\n------Record Not found------\n\n";
				}else{
					cout<<endl<<"No Elements present!"<<endl<<endl;
				}
				break;
			case 5:
				exit(0);
			default:

				break;
		} 
	}
}