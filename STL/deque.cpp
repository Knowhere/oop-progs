#include<iostream>
#include <string.h>
#include <deque>
using namespace std;
class Deque
{
	public:
	
	int a;
	deque <int> d;
	deque <int>::iterator  itr;
	
	void push_front()
	{
		cout<<"Enter the element";
		cin>>a;
		d.push_front(a);
		
	}
	
	void push_back()
	{
		cout<<"Enter the element";
		cin>>a;
		d.push_back(a);
	}
	
	void pop_back()
	{
		itr=d.end();
		itr--;
		d.pop_back();
		cout<<"The element deleted is:"<<*itr;
	}
	
	void pop_front()
	{
		itr=d.begin();
	    d.pop_front();
	    cout<<"The element deleted is:"<<*itr;
	}
	
	void display()
	{
		cout<<"The elements in the Deque are:";
		for(itr=d.begin();itr!=d.end();itr++)
		{
			cout<<*itr;
			cout<<"\n";
		}
	}
};


int main()
{
	int ch;
	char ans;
	Deque obj;
	do 
	{
	cout<<"Enter your choice\n";
	cout<<" 1.Add element from front\n 2.Add element from back\n 3.Delete element from front\n 4.Delete element from back\n 5.Display list";
	cin>>ch;
	  
	  switch(ch)
	  {
		  case 1:
			obj.push_front();
			break;
		  case 2:
			obj.push_back();
			break;
		  case 3:
			obj.pop_front();
			break;
		  case 4:
			obj.pop_back();
			break;
		  case 5:
            obj.display();	
            break;		
			
	  }
       cout<<"Do you want to continue?(y/n)";
       cin>>ans;
   }
   while(ans=='y');
   return 0;
}	  

	    
