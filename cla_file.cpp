#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;

void ReplaceStringInPlace(string& subject, const string& search, const string& replace) {
	int pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

int main(int argc, char const *argv[])
{
	string getcontent;
	char line[50];
	string old_str = argv[1];
	string new_str = argv[2];
	string file_name = argv[3];
	int position = 0, endpos, count = 0, diff;

	if(argc == 4){
		if(old_str.length() == new_str.length()){
			fstream file(argv[3]);
			cout<<"Find string \""<<old_str<<"\" and replace with \""<<new_str<<"\" in file \""<<file_name<<"\"\n";
			fflush(stdin);
			if(file.is_open()){
				while(! file.eof())
				{
					count = 0;
					diff = 0;
					file.getline(line, 50);
					getcontent = line;
					endpos = file.tellp();
					position = getcontent.find(argv[1]);
					if(position != string::npos){
						if(position != -1){
							position = getcontent.find(argv[1]);
							//getcontent.replace(position, old_str.length(), new_str);
							ReplaceStringInPlace(getcontent, old_str, new_str);
							count++;
						}
						diff = old_str.length() - new_str.length();
						diff = diff * count; 
						file.seekp(-(getcontent.length()+1), ios::cur);
						file<<getcontent;
						file.seekg(endpos, ios::beg);
					}
					getcontent.clear();

				}
			}else{
				cout<<"unable to open file";
			}
		}else{
			cout<<"Word lengths do not match";
		}
	}else{
		cout<<"invalid number of parameters";
	}
	return 0;
}