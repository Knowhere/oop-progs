// C++ program to count all subsets with given sum.
#include <iostream>
#include <vector>
#include <stdlib.h>
using namespace std;

int isSubsetSum(int set[], int n, int sum, int count)
{
	// Base Cases
	if (sum == 0)
		return 1;
	if (n == 0 && sum != 0)
		return 0;

	if (set[n-1] > sum){
		if(isSubsetSum(set, n-1, sum, count))
		count++;
	}

	if(isSubsetSum(set, n-1, sum, count))
		count++;
	if(isSubsetSum(set, n-1, sum-set[n-1], count))
		count++;
	return count;
}

int main(int argc, char const *argv[])
{
	int N = atoi(argv[1]);
	int W = atoi(argv[2]);
	int arr[N];
	arr[0] = 1;
	for (int i = 1; i < N; ++i)
	{
		arr[i] = 2*i;
	}
	for (int i = 0; i < N; ++i)
	{
		/* Displaying Weights */
		cout<<arr[i]<<"\t";
	}
	/* Finding Proper subsets */
	cout<<endl<<endl;
	cout<<"Proper Subsets:"<<isSubsetSum(arr, N+1, W, 0);
	cout<<endl<<endl;
}


/*int main(int argc, char const *argv[])
{
	int count = argc - 2;
	int arr[count];
	for (int i = 1; i < count+1; ++i)
	{
		arr[i-1] = atoi(argv[i]);
	}
	int sum = atoi(argv[argc-1]);
	printAllSubsets(arr, count, sum);
	return 0;
}*/