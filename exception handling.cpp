#include<iostream>
#include<string.h>
using namespace std;

class Person{
	int age, wheeler;
	char name[40], city[40];
	long int income;
	public:
	void accept()
	{
		cout<<"Enter Name: \t";
		cin>>name;
		cout<<"\nEnter Age: \t";
		cin>>age;
		cout<<"\nEnter your Income: \t";
		cin>>income;
		cout<<"\nEnter City: \t";
		cin>>city;
		cout<<"\nVehicle type (2/4): \t";
		cin>>wheeler;
		try{
			if(age <= 18 || age >= 55)
				throw age;
			if(income <= 50000 || income >= 100000)
				throw income;
			if(strcmp(city,"Mumbai") != 0 && strcmp(city,"Benglore") != 0 && strcmp(city,"Pune") != 0 && strcmp(city,"chennai") != 0)
				throw city;
			if(wheeler != 4)
				throw wheeler;
			display();
		}catch (...)
		{
			cout<<"Exception Caught!\n";
		}
	}
	
	void display()
	{
		cout<<"Your inputs are:\n";
		cout<<"\nName: \t"<<name;
		cout<<"\nAge: \t"<<age;
		cout<<"\nIncome: \t"<<income;
		cout<<"\nCity: \t"<<city;
		cout<<"\nVehicle type: \t"<<wheeler<<" Wheeler\n";
	}
};

int main()
{
	int choice;
	Person p1;
	p1.accept();
	do{
	cout<<"\n\n1. Display your info \n2. Enter your info \n3. Exit\n\n";
		cout<<"Enter your choice:\t";
		cin>>choice;
		switch(choice){
		case 1:
			p1.display();
			break;
		case 2:
			p1.accept();
			break;
		case 3:
			cout<<"Bye!";
			break;
		default:
			cout<<"Wrong Choice";
		}
	}while(choice != 3);
	return 0;
}
