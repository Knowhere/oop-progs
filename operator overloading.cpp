#include<iostream>
using namespace std;

class Complex{
	public:
		
	int x,y;
	Complex()
	{
		x=0;
		y=0;
	}
		
	Complex operator +(Complex C);
	Complex operator *(Complex C);
	friend ostream &operator<<(ostream &out, Complex &C);
	friend istream &operator<<(istream &in, Complex &C);

};
Complex Complex::operator +(Complex C)
{
	Complex temp;
	temp.x = this->x + C.x;
	temp.y = this->y + C.y;
	return temp;
}

Complex Complex::operator *(Complex C)
{
	Complex temp;
	temp.x = (this->x * C.x) - (this->y * C.y);
	temp.y = (this->x * C.y) + (this->y * C.x);
	return temp;
} 
ostream & operator << (ostream &out, Complex &c)
{
    out << c.x;
    out << " + " << c.y << "i" << endl;
    return out;
}
 
istream & operator >> (istream &in,  Complex &c)
{
    cout << "Enter Real Part: \t";
    in >> c.x;
    cout << "Enter Imagenory Part: \t";
    in >> c.y;
    return in;
}

int main()
{
	Complex C1,C2,C3,C4;
	cin>>C1;
	cin>>C2;
	C3 = C1+C2;
	cout<<"\nAddition is:\t";
	cout<<C3;
	C4 = C1*C2;
	cout<<"\nMultiplication is:\t";
	cout<<C4;
	return 0;
}

