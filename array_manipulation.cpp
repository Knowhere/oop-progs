#include<iostream>
using namespace std;

class array{
	int a[10], b[10], n;
	public:
	void accept()
	{
		cout<<"Enter Size";
		cin>>n;
		cout<<"\nEnter First Array \n";
		for(int i=0;i<n;i++)
			cin>>a[i];
		cout<<"\nEnter Second Array \n";
		for(int i=0;i<n;i++)
			cin>>b[i];
	}
	
	void display()
	{
		cout<<"\nFirst Array: \t";
		for(int i=0;i<n;i++)
			cout<<a[i]<<"\t";
		cout<<"\nSecond Array: \t";
		for(int i=0;i<n;i++)
			cout<<b[i]<<"\t";
	}

	void exchange()
	{
		int temp;
		for(int i=0;i<n;i++)
		{
			temp = b[i];
			b[i] = a[i];
			a[i] = temp;
		}
	}	

	void sort()
	{
		int temp;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n-1;j++)
				if(a[j]>a[j+1])
				{
					temp   = a[j];
					a[j]   = a[j+1];
					a[j+1] = temp;
				}
		}
	
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n-1;j++)
				if(b[j]>b[j+1])
				{
					temp   = b[j];
					b[j]   = b[j+1];
					b[j+1] = temp;
				}
		}
	}
	
	void range()
	{
		cout<<"\n\nRange of First Array is "<<0<<"to"<<n-1<<"\n";
		cout<<"\nRange of Second Array is "<<0<<"to"<<n-1<<"\n";
	}
};

int main()
{
	array a1;
	int choice;
	a1.accept();
	cout<<"\nInitial Arrays: \n";
	a1.display();
	do{
		cout<<"\n\n1. Display Arrays \n2. Sort Arrays \n3. Exchange Arrays \n4. Display Range \n5.Initialize New Arrays \n6. Exit\n\n";
		cout<<"Enter your choice:\t";
		cin>>choice;
		switch(choice){
		case 1:
			cout<<"\nArrays: \n";
			a1.display();
			break;
		case 2:
			a1.sort();
			cout<<"\nArrays Sorted:";
			break;
		case 3:
			a1.exchange();	
			cout<<"\nArrays Exchanged:";
			break;
		case 4:
			a1.range();
			break;
		case 5:
			a1.accept();
			break;
		case 6:
			cout<<"Bye!";
			break;
		default:
			cout<<"Wrong Choice";
		}

	}while(choice != 6);
return 0;
}
