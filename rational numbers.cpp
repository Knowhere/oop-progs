#include <iostream>
using namespace std;

class Fractions
{
public:
	int num, den;
	Fractions(int numerator, int denominator){
		if(denominator >= 1){
			if(numerator % denominator == 0){
				num = numerator/denominator;
				den = 1;
			} else {
				num = numerator;
				den = denominator;
			}
		} else {
			cout<<"Invalid Fraction! Denominator should be a positive Integer greater than 1"<<endl;
		}
	}

	Fractions(){
		num = 0;
		den = 1;
	}
	Fractions operator +(Fractions);
	Fractions operator -(Fractions);
	Fractions operator *(Fractions);
	Fractions operator /(Fractions);
	
	void operator <=(Fractions);
	void operator >=(Fractions);
	void operator ==(Fractions);


	friend ostream &operator<<(ostream &out, Fractions &f);
	friend istream &operator<<(istream &in, Fractions &f);
	void simplify();
	void reciprocal();
};

void Fractions::simplify(){
	if(num % den == 0){
		num = num/den;
		den = 1;
	}else{
		for(int i = den; i>1; i--)
			if(num % i == 0 && den %i == 0){
				num = num /i;
				den = den /i;
				break;
			}
	}
}

void Fractions::reciprocal(){
	int numerator;
	numerator = num;
	num = den;
	den = numerator;
}

Fractions Fractions::operator +(Fractions f2){
	Fractions f3;
	if(this->den != f2.den){
		this->num 	= this->num * f2.den;
		f2.num 		= this->den * f2.num;
		int temp = f2.den * this->den;
		f2.den = temp;
		this->den = temp;
	}
	f3.num = this->num + f2.num;
	f3.den = f2.den;
	f3.simplify();
	return f3;
}

Fractions Fractions::operator -(Fractions f2){
	Fractions f3;
	if(this->den != f2.den){
		this->num 	= this->num * f2.den;
		f2.num 		= this->den * f2.num;
		int temp = f2.den * this->den;
		f2.den = temp;
		this->den = temp;
	}
	f3.num = this->num - f2.num;
	f3.den = f2.den;
	f3.simplify();
	return f3;
}

Fractions Fractions::operator *(Fractions f2){
	Fractions f3;
	f3.num = this->num * f2.num;
	f3.den = this->den * f2.den;
	f3.simplify();
	return f3;
}

Fractions Fractions::operator /(Fractions f2){
	Fractions f3;
	f2.reciprocal();
	f3.num = this->num * f2.num;
	f3.den = this->den * f2.den;
	f3.simplify();
	return f3;
}

ostream & operator << (ostream &out, Fractions &f)
{
	out << f.num;
	if( f.den > 1)
		out << " / " << f.den << endl;
	return out;
}
 
istream & operator >> (istream &in,  Fractions &f)
{
	int numerator, denominator;
	cout << "Enter Numerator Part: \t\t";
	in >> numerator;
	cout << "Enter Denominator Part: \t";
	in >> denominator;
	if(denominator >= 1){
		if(numerator % denominator == 0){
			f.num = numerator/denominator;
			f.den = 1;
		} else {
			f.num = numerator;
			f.den = denominator;
		}
	} else {
		cout<<"Invalid Fraction! Denominator should be a positive Integer greater than 1"<<endl;
		f.num = f.den = 0;
	}
	return in;
}

void Fractions::operator >=(Fractions f2){
	if(this->den != f2.den){
		this->num 	= this->num * f2.den;
		f2.num 		= this->den * f2.num;
		int temp = f2.den * this->den;
		f2.den = temp;
		this->den = temp;
	}
	if(this->num == f2.num){
		simplify();
		f2.simplify();
		cout<<"Both Fractions are Equal."<<endl;
	}else if(this->num > f2.num){
		simplify();
		f2.simplify();
		cout<<"Fraction 1 is greater than Fraction 2."<<endl;
	}else{
		simplify();
		f2.simplify(); 
		cout<<"Fraction 1 is smaller than Fraction 2."<<endl;
	}
}

void Fractions::operator <=(Fractions f2){
	if(this->den != f2.den){
		this->num 	= this->num * f2.den;
		f2.num 		= this->den * f2.num;
		int temp = f2.den * this->den;
		f2.den = temp;
		this->den = temp;
	}
	if(this->num == f2.num){
		simplify();
		f2.simplify();
		cout<<"Both Fractions are Equal."<<endl;
	}else if(this->num < f2.num){
		simplify();
		f2.simplify();
		cout<<"Fraction 1 is smaller than Fraction 2."<<endl;
	}else{
		simplify();
		f2.simplify(); 
		cout<<"Fraction 1 is greater than Fraction 2."<<endl;
	}
}


void Fractions::operator ==(Fractions f2){
	if(this->den != f2.den){
		this->num 	= this->num * f2.den;
		f2.num 		= this->den * f2.num;
		int temp = f2.den * this->den;
		f2.den = temp;
		this->den = temp;
	}
	if(this->num == f2.num){
		simplify();
		f2.simplify();
		cout<<"Both Fractions are Equal."<<endl;
	}else{
		simplify();
		f2.simplify(); 
		cout<<"Both Fractions are Unequal."<<endl;
	}
}


int main()
{
	Fractions f1, f2, f3;
	cin>>f1;
	cin>>f2;
	cout<<"Acccepted values are :"<<endl;
	cout<<f1<<endl;
	cout<<f2<<endl;
	cout<<"Result of Addition is :\t";
	f3 = f1 + f2;
	cout<<f3<<endl;
	cout<<"Result of Subtraction is :\t";
	f3 = f1 - f2;
	cout<<f3<<endl;
	cout<<"Result of Multiplication is :\t";
	f3 = f1 * f2;
	cout<<f3<<endl;
	cout<<"Result of Division is :\t";
	f3 = f1 / f2;
	cout<<f3<<endl;
	cout<<"Relational Operator overloading:"<<endl;
	cout<<"Fraction 1 <= Fraction 2 :";
	f1<=f2;
	cout<<"Fraction 1 >= Fraction 2 :";
	f1>=f2;
	cout<<"Fraction 1 == Fraction 2 :";
	f1==f2;

	cout<<"Program Terminated"<<endl;
	return 0;
}